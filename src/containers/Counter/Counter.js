import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import * as resultActionCreators from '../../store/actions/index';

import CounterControl from '../../components/CounterControl/CounterControl';
import CounterOutput from '../../components/CounterOutput/CounterOutput';
import * as counterActionCreators from "../../store/actions/counterAction";

const Counter = props => {

    const ctr = useSelector(state => state.ctr.counter);
    const storedResults = useSelector(state => state.res.results);

    const dispatch = useDispatch();
    const onIncrementCounter = () => dispatch(counterActionCreators.increment());
    const onDecrementCounter =  () => dispatch(counterActionCreators.decrement());
    const onAdd = () => dispatch(counterActionCreators.add(10));
    const onSub = () => dispatch(counterActionCreators.sub(15));
    const onStoreResult = (result) => dispatch(resultActionCreators.store_result(result));
    const onDeleteResult = (id) => dispatch(resultActionCreators.delete_result(id));

        return (
            <div>
                {/*usuario: {ctr.name}  pais: {ctr.country}*/}
                <CounterOutput value={ctr} /> {/*was state.counterReducer*/}
                <CounterControl label="Increment" clicked={onIncrementCounter} /> {/*drop parenthesis from end of function so it's just referenced not called*/}
                <CounterControl label="Decrement" clicked={onDecrementCounter} />
                <CounterControl label="Add" clicked={onAdd} />
                <CounterControl label="Sub" clicked={onSub} />
                <hr />
                <button onClick={() => onStoreResult(ctr)}>Store Result</button>
                <ul>
                    {storedResults.map(strResult => (
                        <li key={strResult.id} onClick={() => onDeleteResult(strResult.id)}>{strResult.value}</li>
                        /* onClick=()=>'fn' vs just 'fn' prevents fn from executing when component renders  */
                    ))}
                </ul>
            </div>
        );
}

export default Counter;