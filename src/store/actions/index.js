//export all functions in current folder used in other files
export {
    add,
    sub,
    increment,
    decrement
} from './counterAction'
export {
    store_result,
    delete_result
} from './resultAction'