import { combineReducers} from 'redux';
import counterReducer from "./counterReducer";
import results from "./resultReducer";

export default combineReducers({
    ctr: counterReducer,
    res: results
})