import * as actionTypes from "../actions/actionTypes";
import {updateObject} from "../utility";

const initialState = {
    results: []
}

const deleteResult = (state, action) => { //case statement helper function
    const newArray = state.results.filter(result => result.id!==action.resultElId)
    return updateObject(state, {results: newArray})
}

const results = (state=initialState, action) => {
    switch (action.type) {
        case actionTypes.STORE_RESULT:
            const updateResultRecommended = 2 * action.result //do data tranforms here in reducer & aysc in ActionCreator
            return updateObject(
                state,
                {results: state.results.concat({id: new Date(), value: updateResultRecommended} )}
                )
        case actionTypes.DELETE_RESULT:
            return deleteResult(state, action)
        default: return state;
    }
};

export default results;